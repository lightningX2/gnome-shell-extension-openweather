# Polish translation for gnome-shell-extension-openweather.
# Copyright © 2011-2016 the gnome-shell-extension-openweather authors.
# This file is distributed under the same license as the gnome-shell-extension-openweather package.
# Piotr Sokół <psokol@jabster.pl>, 2011, 2012.
# Marcin Niechciał <bastard79@o2.pl>, 2015.
# Aviary.pl <community-poland@mozilla.org>, 2015, 2016.
# Piotr Drąg <piotrdrag@gmail.com>, 2015, 2016, 2017.
#
msgid ""
msgstr ""
"Project-Id-Version: gnome-shell-extension-openweather\n"
"Report-Msgid-Bugs-To: https://gitlab.com/jenslody/gnome-shell-extension-"
"openweather/issues\n"
"POT-Creation-Date: 2021-05-09 10:46+0200\n"
"PO-Revision-Date: 2017-02-04 20:28+0100\n"
"Last-Translator: Piotr Drąg <piotrdrag@gmail.com>\n"
"Language-Team: Polish <community-poland@mozilla.org>\n"
"Language: pl\n"
"MIME-Version: 1.0\n"
"Content-Type: text/plain; charset=UTF-8\n"
"Content-Transfer-Encoding: 8bit\n"
"Plural-Forms: nplurals=3; plural=(n==1 ? 0 : n%10>=2 && n%10<=4 && (n%100<10 "
"|| n%100>=20) ? 1 : 2);\n"
"X-Generator: Gtranslator 2.91.7\n"

#: src/extension.js:181
msgid "..."
msgstr "…"

#: src/extension.js:360
msgid ""
"Openweathermap.org does not work without an api-key.\n"
"Either set the switch to use the extensions default key in the preferences "
"dialog to on or register at https://openweathermap.org/appid and paste your "
"personal key into the preferences dialog."
msgstr ""
"Serwis openweathermap.org nie działa bez klucza API.\n"
"Proszę włączyć używanie domyślnego klucza rozszerzenia w oknie preferencji "
"lub zarejestrować się na stronie https://openweathermap.org/appid i wkleić "
"osobisty klucz w oknie preferencji."

#: src/extension.js:414
msgid ""
"Dark Sky does not work without an api-key.\n"
"Please register at https://darksky.net/dev/register and paste your personal "
"key into the preferences dialog."
msgstr ""
"Serwis Dark Sky nie działa bez klucza API.\n"
"Proszę zarejestrować się na stronie https://darksky.net/dev/register "
"i wkleić osobisty klucz w oknie preferencji."

#: src/extension.js:519 src/extension.js:531
#, javascript-format
msgid "Can not connect to %s"
msgstr "Nie można połączyć z %s"

#: src/extension.js:843 data/weather-settings.ui:300
msgid "Locations"
msgstr "Położenia"

#: src/extension.js:855
msgid "Reload Weather Information"
msgstr "Ponownie wczytaj komunikat pogodowy"

#: src/extension.js:870
msgid "Weather data provided by:"
msgstr "Dane o pogodzie dostarcza:"

#: src/extension.js:880
#, javascript-format
msgid "Can not open %s"
msgstr "Nie można otworzyć %s"

#: src/extension.js:887
msgid "Weather Settings"
msgstr "Ustawienia pogody"

#: src/extension.js:938 src/prefs.js:1059
msgid "Invalid city"
msgstr "Nieprawidłowa nazwa miasta"

#: src/extension.js:949
msgid "Invalid location! Please try to recreate it."
msgstr "Nieprawidłowe położenie. Proszę spróbować jeszcze raz."

#: src/extension.js:1000 data/weather-settings.ui:567
msgid "°F"
msgstr "°F"

#: src/extension.js:1002 data/weather-settings.ui:568
msgid "K"
msgstr "K"

#: src/extension.js:1004 data/weather-settings.ui:569
msgid "°Ra"
msgstr "°Ra"

#: src/extension.js:1006 data/weather-settings.ui:570
msgid "°Ré"
msgstr "°Ré"

#: src/extension.js:1008 data/weather-settings.ui:571
msgid "°Rø"
msgstr "°Rø"

#: src/extension.js:1010 data/weather-settings.ui:572
msgid "°De"
msgstr "°De"

#: src/extension.js:1012 data/weather-settings.ui:573
msgid "°N"
msgstr "°N"

#: src/extension.js:1014 data/weather-settings.ui:566
msgid "°C"
msgstr "°C"

#: src/extension.js:1055
msgid "Calm"
msgstr "cisza"

#: src/extension.js:1058
msgid "Light air"
msgstr "słaby wiatr"

#: src/extension.js:1061
msgid "Light breeze"
msgstr "słaba bryza"

#: src/extension.js:1064
msgid "Gentle breeze"
msgstr "łagodna bryza"

#: src/extension.js:1067
msgid "Moderate breeze"
msgstr "umiarkowana bryza"

#: src/extension.js:1070
msgid "Fresh breeze"
msgstr "rześka bryza"

#: src/extension.js:1073
msgid "Strong breeze"
msgstr "silna bryza"

#: src/extension.js:1076
msgid "Moderate gale"
msgstr "umiarkowany wicher"

#: src/extension.js:1079
msgid "Fresh gale"
msgstr "rześki wicher"

#: src/extension.js:1082
msgid "Strong gale"
msgstr "silny wicher"

#: src/extension.js:1085
msgid "Storm"
msgstr "burza"

#: src/extension.js:1088
msgid "Violent storm"
msgstr "gwałtowna burza"

#: src/extension.js:1091
msgid "Hurricane"
msgstr "huragan"

#: src/extension.js:1095
msgid "Sunday"
msgstr "niedziela"

#: src/extension.js:1095
msgid "Monday"
msgstr "poniedziałek"

#: src/extension.js:1095
msgid "Tuesday"
msgstr "wtorek"

#: src/extension.js:1095
msgid "Wednesday"
msgstr "środa"

#: src/extension.js:1095
msgid "Thursday"
msgstr "czwartek"

#: src/extension.js:1095
msgid "Friday"
msgstr "piątek"

#: src/extension.js:1095
msgid "Saturday"
msgstr "sobota"

#: src/extension.js:1101
msgid "N"
msgstr "pn."

#: src/extension.js:1101
msgid "NE"
msgstr "pn.-wsch."

#: src/extension.js:1101
msgid "E"
msgstr "wsch."

#: src/extension.js:1101
msgid "SE"
msgstr "pd.-wsch."

#: src/extension.js:1101
msgid "S"
msgstr "pd."

#: src/extension.js:1101
msgid "SW"
msgstr "pd.-zach."

#: src/extension.js:1101
msgid "W"
msgstr "zach."

#: src/extension.js:1101
msgid "NW"
msgstr "pn.-zach."

#: src/extension.js:1174 src/extension.js:1183 data/weather-settings.ui:600
msgid "hPa"
msgstr "hPa"

#: src/extension.js:1178 data/weather-settings.ui:601
msgid "inHg"
msgstr "inHg"

#: src/extension.js:1188 data/weather-settings.ui:602
msgid "bar"
msgstr "bar"

#: src/extension.js:1193 data/weather-settings.ui:603
msgid "Pa"
msgstr "Pa"

#: src/extension.js:1198 data/weather-settings.ui:604
msgid "kPa"
msgstr "kPa"

#: src/extension.js:1203 data/weather-settings.ui:605
msgid "atm"
msgstr "atm"

#: src/extension.js:1208 data/weather-settings.ui:606
msgid "at"
msgstr "at"

#: src/extension.js:1213 data/weather-settings.ui:607
msgid "Torr"
msgstr "Tr"

#: src/extension.js:1218 data/weather-settings.ui:608
msgid "psi"
msgstr "psi"

#: src/extension.js:1223 data/weather-settings.ui:609
msgid "mmHg"
msgstr "mmHg"

#: src/extension.js:1228 data/weather-settings.ui:610
msgid "mbar"
msgstr "mbar"

#: src/extension.js:1272 data/weather-settings.ui:586
msgid "m/s"
msgstr "m/s"

#: src/extension.js:1276 data/weather-settings.ui:585
msgid "mph"
msgstr "mph"

#: src/extension.js:1281 data/weather-settings.ui:584
msgid "km/h"
msgstr "km/h"

#: src/extension.js:1290 data/weather-settings.ui:587
msgid "kn"
msgstr "w."

#: src/extension.js:1295 data/weather-settings.ui:588
msgid "ft/s"
msgstr "ft/s"

#: src/extension.js:1389
msgid "Loading ..."
msgstr "Wczytywanie…"

#: src/extension.js:1393
msgid "Please wait"
msgstr "Proszę czekać"

#: src/extension.js:1454
msgid "Cloudiness:"
msgstr "Zachmurzenie:"

#: src/extension.js:1458
msgid "Humidity:"
msgstr "Wilgotność względna:"

#: src/extension.js:1462
msgid "Pressure:"
msgstr "Ciśnienie atmosferyczne:"

#: src/extension.js:1466
msgid "Wind:"
msgstr "Kierunek, prędkość wiatru:"

#: src/darksky_net.js:159 src/darksky_net.js:291 src/openweathermap_org.js:352
#: src/openweathermap_org.js:479
msgid "Yesterday"
msgstr "Wczoraj"

#: src/darksky_net.js:162 src/darksky_net.js:294 src/openweathermap_org.js:354
#: src/openweathermap_org.js:481
#, javascript-format
msgid "%d day ago"
msgid_plural "%d days ago"
msgstr[0] "%d dzień temu"
msgstr[1] "%d dni temu"
msgstr[2] "%d dni temu"

#: src/darksky_net.js:174 src/darksky_net.js:176 src/openweathermap_org.js:368
#: src/openweathermap_org.js:370
msgid ", "
msgstr ", "

#: src/darksky_net.js:271 src/openweathermap_org.js:473
msgid "Today"
msgstr "Dzisiaj"

#: src/darksky_net.js:287 src/openweathermap_org.js:475
msgid "Tomorrow"
msgstr "Jutro"

#: src/darksky_net.js:289 src/openweathermap_org.js:477
#, javascript-format
msgid "In %d day"
msgid_plural "In %d days"
msgstr[0] "Za %d dzień"
msgstr[1] "Za %d dni"
msgstr[2] "Za %d dni"

#: src/openweathermap_org.js:183
msgid "Thunderstorm with light rain"
msgstr "burza z piorunami z lekkimi opadami deszczu"

#: src/openweathermap_org.js:185
msgid "Thunderstorm with rain"
msgstr "burza z piorunami z opadami deszczu"

#: src/openweathermap_org.js:187
msgid "Thunderstorm with heavy rain"
msgstr "burza z piorunami z silnymi opadami deszczu"

#: src/openweathermap_org.js:189
msgid "Light thunderstorm"
msgstr "mała burza z piorunami"

#: src/openweathermap_org.js:191
msgid "Thunderstorm"
msgstr "burza z piorunami"

#: src/openweathermap_org.js:193
msgid "Heavy thunderstorm"
msgstr "silna burza z piorunami"

#: src/openweathermap_org.js:195
msgid "Ragged thunderstorm"
msgstr "burza z piorunami"

#: src/openweathermap_org.js:197
msgid "Thunderstorm with light drizzle"
msgstr "burza z piorunami i słabą mżawką"

#: src/openweathermap_org.js:199
msgid "Thunderstorm with drizzle"
msgstr "burza z piorunami i mżawką"

#: src/openweathermap_org.js:201
msgid "Thunderstorm with heavy drizzle"
msgstr "burze z piorunami z silną mżawką"

#: src/openweathermap_org.js:203
msgid "Light intensity drizzle"
msgstr "słaba mżawka"

#: src/openweathermap_org.js:205
msgid "Drizzle"
msgstr "mżawka"

#: src/openweathermap_org.js:207
msgid "Heavy intensity drizzle"
msgstr "silna mżawka"

#: src/openweathermap_org.js:209
msgid "Light intensity drizzle rain"
msgstr "słabe opady deszczu i mżawka"

#: src/openweathermap_org.js:211
msgid "Drizzle rain"
msgstr "mżawka"

#: src/openweathermap_org.js:213
msgid "Heavy intensity drizzle rain"
msgstr "silne opady deszczu i mżawka"

#: src/openweathermap_org.js:215
msgid "Shower rain and drizzle"
msgstr "przelotne opady deszczu i mżawka"

#: src/openweathermap_org.js:217
msgid "Heavy shower rain and drizzle"
msgstr "silne, przelotne opady deszczu i mżawka"

#: src/openweathermap_org.js:219
msgid "Shower drizzle"
msgstr "przelotna mżawka"

#: src/openweathermap_org.js:221
msgid "Light rain"
msgstr "słabe opady deszczu"

#: src/openweathermap_org.js:223
msgid "Moderate rain"
msgstr "umiarkowane opady deszczu"

#: src/openweathermap_org.js:225
msgid "Heavy intensity rain"
msgstr "silne opady deszczu"

#: src/openweathermap_org.js:227
msgid "Very heavy rain"
msgstr "silne opady deszczu"

#: src/openweathermap_org.js:229
msgid "Extreme rain"
msgstr "ulewa"

#: src/openweathermap_org.js:231
msgid "Freezing rain"
msgstr "marznący deszcz"

#: src/openweathermap_org.js:233
msgid "Light intensity shower rain"
msgstr "słabe, przelotne opady deszczu"

#: src/openweathermap_org.js:235
msgid "Shower rain"
msgstr "przelotne opady deszczu"

#: src/openweathermap_org.js:237
msgid "Heavy intensity shower rain"
msgstr "silne, przelotne opady śniegu"

#: src/openweathermap_org.js:239
msgid "Ragged shower rain"
msgstr "opady deszczu"

#: src/openweathermap_org.js:241
msgid "Light snow"
msgstr "słabe opady śniegu"

#: src/openweathermap_org.js:243
msgid "Snow"
msgstr "śnieg"

#: src/openweathermap_org.js:245
msgid "Heavy snow"
msgstr "silne opady śniegu"

#: src/openweathermap_org.js:247
msgid "Sleet"
msgstr "deszcz ze śniegiem"

#: src/openweathermap_org.js:249
msgid "Shower sleet"
msgstr "przelotne opady deszczu ze śniegiem"

#: src/openweathermap_org.js:251
msgid "Light rain and snow"
msgstr "słabe opady deszczu ze śniegiem"

#: src/openweathermap_org.js:253
msgid "Rain and snow"
msgstr "deszcz ze śniegiem"

#: src/openweathermap_org.js:255
msgid "Light shower snow"
msgstr "słabe, przelotne opady śniegu"

#: src/openweathermap_org.js:257
msgid "Shower snow"
msgstr "przelotne opady śniegu"

#: src/openweathermap_org.js:259
msgid "Heavy shower snow"
msgstr "silne opady śniegu"

#: src/openweathermap_org.js:261
msgid "Mist"
msgstr "mgła"

#: src/openweathermap_org.js:263
msgid "Smoke"
msgstr "dym"

#: src/openweathermap_org.js:265
msgid "Haze"
msgstr "mgła"

#: src/openweathermap_org.js:267
msgid "Sand/Dust Whirls"
msgstr "wiry piasku/pyłu"

#: src/openweathermap_org.js:269
msgid "Fog"
msgstr "mgła"

#: src/openweathermap_org.js:271
msgid "Sand"
msgstr "piasek"

#: src/openweathermap_org.js:273
msgid "Dust"
msgstr "pył"

#: src/openweathermap_org.js:275
msgid "VOLCANIC ASH"
msgstr "pył wulkaniczny"

#: src/openweathermap_org.js:277
msgid "SQUALLS"
msgstr "szkwał"

#: src/openweathermap_org.js:279
msgid "TORNADO"
msgstr "tornado"

#: src/openweathermap_org.js:281
msgid "Sky is clear"
msgstr "bezchmurnie"

#: src/openweathermap_org.js:283
msgid "Few clouds"
msgstr "kilka chmur"

#: src/openweathermap_org.js:285
msgid "Scattered clouds"
msgstr "mało chmur"

#: src/openweathermap_org.js:287
msgid "Broken clouds"
msgstr "dużo chmur"

#: src/openweathermap_org.js:289
msgid "Overcast clouds"
msgstr "całkowite zachmurzenie"

#: src/openweathermap_org.js:291
msgid "Not available"
msgstr "Niedostępne"

#: src/openweathermap_org.js:384
msgid "?"
msgstr ""

#: src/prefs.js:197
#, fuzzy
msgid "Searching ..."
msgstr "Wczytywanie…"

#: src/prefs.js:209 src/prefs.js:247 src/prefs.js:278 src/prefs.js:282
#, javascript-format
msgid "Invalid data when searching for \"%s\""
msgstr "Nieprawidłowe dane podczas wyszukiwania „%s”"

#: src/prefs.js:214 src/prefs.js:254 src/prefs.js:285
#, javascript-format
msgid "\"%s\" not found"
msgstr "Nie odnaleziono „%s”"

#: src/prefs.js:232
#, fuzzy
msgid "You need an AppKey to search on openmapquest."
msgstr "Osobisty klucz aplikacji z serwisu developer.mapquest.com"

#: src/prefs.js:233
#, fuzzy
msgid "Please visit https://developer.mapquest.com/ ."
msgstr "Osobisty klucz aplikacji z serwisu developer.mapquest.com"

#: src/prefs.js:248
msgid "Do you use a valid AppKey to search on openmapquest ?"
msgstr ""

#: src/prefs.js:249
msgid "If not, please visit https://developer.mapquest.com/ ."
msgstr ""

#: src/prefs.js:339
msgid "Location"
msgstr "Położenie"

#: src/prefs.js:350
msgid "Provider"
msgstr "Dostawca"

#: src/prefs.js:360
msgid "Result"
msgstr ""

#: src/prefs.js:550
#, javascript-format
msgid "Remove %s ?"
msgstr "Usunąć %s?"

#: src/prefs.js:563
#, fuzzy
msgid "No"
msgstr "pn."

#: src/prefs.js:564
msgid "Yes"
msgstr ""

#: src/prefs.js:1094
msgid "default"
msgstr "domyślne"

#: data/weather-settings.ui:25
msgid "Edit name"
msgstr "Nazwa"

#: data/weather-settings.ui:36 data/weather-settings.ui:52
#: data/weather-settings.ui:178
msgid "Clear entry"
msgstr "Usuń wpis"

#: data/weather-settings.ui:43
msgid "Edit coordinates"
msgstr "Współrzędne"

#: data/weather-settings.ui:59 data/weather-settings.ui:195
msgid "Extensions default weather provider"
msgstr "Domyślny dostawca informacji o pogodzie"

#: data/weather-settings.ui:78 data/weather-settings.ui:214
msgid "Cancel"
msgstr "Anuluj"

#: data/weather-settings.ui:88 data/weather-settings.ui:224
msgid "Save"
msgstr "Zapisz"

#: data/weather-settings.ui:164
msgid "Search by location or coordinates"
msgstr "Położenie lub współrzędne"

#: data/weather-settings.ui:179
msgid "e.g. Vaiaku, Tuvalu or -8.5211767,179.1976747"
msgstr "np. Warszawa, mazowieckie, Polska lub 52.2319237,21.0067265"

#: data/weather-settings.ui:184
msgid "Find"
msgstr "Znajdź"

#: data/weather-settings.ui:318
msgid "Chose default weather provider"
msgstr "Domyślny dostawca informacji o pogodzie"

#: data/weather-settings.ui:329
msgid "Personal Api key from openweathermap.org"
msgstr "Osobisty klucz API z serwisu openweathermap.org"

#: data/weather-settings.ui:372
msgid "Personal Api key from Dark Sky"
msgstr "Osobisty klucz API z serwisu Dark Sky"

#: data/weather-settings.ui:383
msgid "Refresh timeout for current weather [min]"
msgstr "Aktualizacja informacji o obecnej pogodzie (w minutach)"

#: data/weather-settings.ui:395
msgid "Refresh timeout for weather forecast [min]"
msgstr "Aktualizacja informacji o prognozie pogody (w minutach)"

#: data/weather-settings.ui:418
msgid ""
"Note: the forecast-timout is not used for Dark Sky, because they do not "
"provide seperate downloads for current weather and forecasts."
msgstr ""
"Uwaga: ustawienia aktualizacji nie działają z serwisem Dark Sky, ponieważ "
"nie ma możliwości pobrania informacji osobno dla obecnej pogody i prognozy."

#: data/weather-settings.ui:441
msgid "Use extensions api-key for openweathermap.org"
msgstr "Klucz API rozszerzenia dla serwisu openweathermap.org"

#: data/weather-settings.ui:450
msgid ""
"Switch off, if you have your own api-key for openweathermap.org and put it "
"into the text-box below."
msgstr ""
"Należy wyłączyć, jeżeli w polu poniżej wklejono osobisty klucz dla serwisu "
"openweathermap.org."

#: data/weather-settings.ui:462
msgid "Weather provider"
msgstr "Dostawca informacji o pogodzie"

#: data/weather-settings.ui:478
msgid "Chose geolocation provider"
msgstr "Dostawca geolokalizacji"

#: data/weather-settings.ui:500
msgid "Personal AppKey from developer.mapquest.com"
msgstr "Osobisty klucz aplikacji z serwisu developer.mapquest.com"

#: data/weather-settings.ui:522
msgid "Geolocation provider"
msgstr "Dostawca geolokalizacji"

#: data/weather-settings.ui:538
#: data/org.gnome.shell.extensions.openweather.gschema.xml:63
msgid "Temperature Unit"
msgstr "Jednostka temperatury"

#: data/weather-settings.ui:547
msgid "Wind Speed Unit"
msgstr "Jednostka prędkości wiatru"

#: data/weather-settings.ui:556
#: data/org.gnome.shell.extensions.openweather.gschema.xml:67
msgid "Pressure Unit"
msgstr "Jednostka ciśnienia"

#: data/weather-settings.ui:589
msgid "Beaufort"
msgstr "B"

#: data/weather-settings.ui:622
msgid "Units"
msgstr "Jednostki"

#: data/weather-settings.ui:638
#: data/org.gnome.shell.extensions.openweather.gschema.xml:113
msgid "Position in Panel"
msgstr "Położenie na panelu"

#: data/weather-settings.ui:647
msgid "Position of menu-box [%] from 0 (left) to 100 (right)"
msgstr "Położenie menu (w procentach) od 0 (po lewej) do 100 (po prawej)"

#: data/weather-settings.ui:656
#: data/org.gnome.shell.extensions.openweather.gschema.xml:76
msgid "Wind Direction by Arrows"
msgstr "Strzałka kierunku wiatru"

#: data/weather-settings.ui:665
#: data/org.gnome.shell.extensions.openweather.gschema.xml:89
msgid "Translate Conditions"
msgstr "Tłumaczenia warunków pogodowych"

#: data/weather-settings.ui:674
#: data/org.gnome.shell.extensions.openweather.gschema.xml:93
msgid "Symbolic Icons"
msgstr "Ikony symboliczne"

#: data/weather-settings.ui:683
msgid "Text on buttons"
msgstr "Tekst na przyciskach"

#: data/weather-settings.ui:692
#: data/org.gnome.shell.extensions.openweather.gschema.xml:101
msgid "Temperature in Panel"
msgstr "Temperatura na panelu"

#: data/weather-settings.ui:701
#: data/org.gnome.shell.extensions.openweather.gschema.xml:105
msgid "Conditions in Panel"
msgstr "Warunki pogodowe na panelu"

#: data/weather-settings.ui:710
#: data/org.gnome.shell.extensions.openweather.gschema.xml:109
msgid "Conditions in Forecast"
msgstr "Warunki pogodowe w prognozie"

#: data/weather-settings.ui:719
msgid "Center forecast"
msgstr "Wyśrodkowanie prognozy"

#: data/weather-settings.ui:728
#: data/org.gnome.shell.extensions.openweather.gschema.xml:137
msgid "Number of days in forecast"
msgstr "Liczba dni prognozy"

#: data/weather-settings.ui:737
#: data/org.gnome.shell.extensions.openweather.gschema.xml:141
msgid "Maximal number of digits after the decimal point"
msgstr "Maksymalna liczba cyfr po przecinku"

#: data/weather-settings.ui:747
msgid "Center"
msgstr "Na środku"

#: data/weather-settings.ui:748
msgid "Right"
msgstr "Po prawej"

#: data/weather-settings.ui:749
msgid "Left"
msgstr "Po lewej"

#: data/weather-settings.ui:880
#: data/org.gnome.shell.extensions.openweather.gschema.xml:125
msgid "Maximal length of the location text"
msgstr ""

#: data/weather-settings.ui:902
msgid "Layout"
msgstr "Wygląd"

#: data/weather-settings.ui:935
msgid "Version: "
msgstr "Wersja: "

#: data/weather-settings.ui:941
msgid "unknown (self-build ?)"
msgstr "nieznana"

#: data/weather-settings.ui:949
msgid ""
"<span>Weather extension to display weather information from <a href="
"\"https://openweathermap.org/\">Openweathermap</a> or <a href=\"https://"
"darksky.net\">Dark Sky</a> for almost all locations in the world.</span>"
msgstr ""
"<span>Rozszerzenie pogody pobiera informacje z serwisu <a href=\"https://"
"openweathermap.org/\">OpenWeatherMap</a> lub <a href=\"https://darksky.net"
"\">Dark Sky</a> dla niemal wszystkich miejsc na świecie.</span>"

#: data/weather-settings.ui:963
msgid "Maintained by"
msgstr "Autor:"

#: data/weather-settings.ui:976
msgid "Webpage"
msgstr "Strona WWW"

#: data/weather-settings.ui:986
msgid ""
"<span size=\"small\">This program comes with ABSOLUTELY NO WARRANTY.\n"
"See the <a href=\"https://www.gnu.org/licenses/old-licenses/gpl-2.0.html"
"\">GNU General Public License, version 2 or later</a> for details.</span>"
msgstr ""
"<span size=\"small\">Niniejszy program rozpowszechniany jest BEZ "
"JAKIEJKOLWIEK GWARANCJI.\n"
"Więcej informacji: <a href=\"https://www.gnu.org/licenses/old-licenses/"
"gpl-2.0.html\">Powszechna licencja publiczna GNU, wersja 2 lub późniejsza</"
"a>.</span>"

#: data/weather-settings.ui:997
msgid "About"
msgstr "O programie"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:55
msgid "Weather Provider"
msgstr "Dostawca informacji o pogodzie"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:59
msgid "Geolocation Provider"
msgstr "Dostawca geolokalizacji"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:71
msgid "Wind Speed Units"
msgstr "Jednostka prędkości wiatru"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:72
msgid ""
"Choose the units used for wind speed. Allowed values are 'kph', 'mph', 'm/"
"s', 'knots', 'ft/s' or 'Beaufort'."
msgstr ""
"Jednostka używana dla prędkości wiatru. Dozwolone wartości: „kph”, „mph”, „m/"
"s”, „knots”, „ft/s” lub „Beaufort”."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:77
msgid "Choose whether to display wind direction through arrows or letters."
msgstr "Czy wyświetlać kierunek wiatru za pomocą strzałek lub liter."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:81
msgid "City to be displayed"
msgstr "Wyświetlane miasto"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:85
msgid "Actual City"
msgstr "Obecne miasto"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:97
msgid "Use text on buttons in menu"
msgstr "Tekst na przyciskach w menu"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:117
msgid "Horizontal position of menu-box."
msgstr "Położenie menu w poziomie."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:121
msgid "Refresh interval (actual weather)"
msgstr "Czas odświeżania (obecnej pogody)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:129
msgid "Refresh interval (forecast)"
msgstr "Czas odświeżania (prognozy)"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:133
msgid "Center forecastbox."
msgstr "Wyśrodkowanie prognozy."

#: data/org.gnome.shell.extensions.openweather.gschema.xml:145
msgid "Your personal API key from openweathermap.org"
msgstr "Osobisty klucz API z serwisu openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:149
msgid "Use the extensions default API key from openweathermap.org"
msgstr "Użycie domyślnego klucza API rozszerzenia z serwisu openweathermap.org"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:153
msgid "Your personal API key from Dark Sky"
msgstr "Osobisty klucz API z serwisu Dark Sky"

#: data/org.gnome.shell.extensions.openweather.gschema.xml:157
msgid "Your personal AppKey from developer.mapquest.com"
msgstr "Osobisty klucz aplikacji z serwisu developer.mapquest.com"
